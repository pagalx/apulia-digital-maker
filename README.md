# ITS Apulia Digital Maker

🇮🇹 Nell'ambito del percorso formativo "Sviluppo App cross-platform", la presente repository ripropone alcuni degli esempi illustrati nell'ebook 
**[Testing Angular – A Guide to Robust Angular Applications
](https://testing-angular.com/)** 📖 a puro scopo didattico.

🇬🇧 As part of the training course "Cross-platoform App Development", this repository proposes some of the examples illustrated in the ebook **[Testing Angular – A Guide to Robust Angular Applications
](https://testing-angular.com/)** 📖 for purely educational purposes.

## Credits
- [9elements](https://github.com/9elements)
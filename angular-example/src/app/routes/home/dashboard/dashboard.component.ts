import { Component, OnInit } from '@angular/core'
import { AuthService } from '@auth/services'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
  constructor(auth: AuthService) {
    console.log(auth.getUser())
  }
}

import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { AuthModule } from '@auth/auth.module'
import { Authguard } from './guards/_auth.guard'
import { Unauthguard } from './guards/_unauth.guard'
import { LoginComponent } from './login/login.component'

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [Unauthguard],
  },
  {
    path: '',
    canActivate: [Authguard],
    loadChildren: () =>
      import('./home/home.module').then((Module) => Module.HomeModule),
  },
]

@NgModule({
  declarations: [LoginComponent],
  imports: [AuthModule, RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class RoutesModule {}

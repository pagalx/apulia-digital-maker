import { Injectable } from '@angular/core'
import { CanActivate, Router, UrlTree } from '@angular/router'
import { AuthService } from '@auth/services'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root',
})
export class Authguard implements CanActivate {
  constructor(
    private readonly _auth: AuthService,
    private readonly _router: Router,
  ) {}

  canActivate(): Observable<boolean | UrlTree> {
    return this._auth.isLogged$.pipe(
      map((isLogged) => {
        if (isLogged) {
          return true
        }

        this._router.navigate(['login'])
        return false
      }),
    )
  }
}

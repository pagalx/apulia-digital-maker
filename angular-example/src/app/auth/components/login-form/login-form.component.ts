import { Component } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms'
import { LoginForm } from '@auth/@types'
import { AuthService } from '@auth/services'

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent {
  hide = true

  form

  constructor(private readonly _auth: AuthService) {
    this.form = new FormGroup<LoginForm>({
      username: new FormControl<string>('', {
        validators: [Validators.required, Validators.email],
        nonNullable: true,
      }),
      password: new FormControl<string>('', {
        validators: Validators.required,
        nonNullable: true,
      }),
    })
  }

  login(): void {
    if (this.form.status === 'INVALID') {
      return
    }

    const { username } = this.form.getRawValue()
    this._auth.login(username)
  }
}

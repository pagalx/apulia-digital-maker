import { ComponentFixture, TestBed } from '@angular/core/testing'
import { ReactiveFormsModule } from '@angular/forms'
import { MatButtonModule } from '@angular/material/button'
import { MatIconModule } from '@angular/material/icon'
import { MatInputModule } from '@angular/material/input'
import { BrowserModule, By } from '@angular/platform-browser'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { RouterModule } from '@angular/router'
import { RouterTestingModule } from '@angular/router/testing'
import { AuthService } from '@auth/services'
import { of } from 'rxjs'

import { LoginFormComponent } from './login-form.component'

const USERNAME = 'alessandropagliara88@gmail.com'
const PASSWORD = '123'

describe('LoginFormComponent', () => {
  let component: LoginFormComponent
  let fixture: ComponentFixture<LoginFormComponent>
  let authService: jasmine.SpyObj<AuthService>

  beforeEach(async () => {
    authService = jasmine.createSpyObj<AuthService>('AuthService', {
      isLogged$: of(false),
      login: undefined,
    })

    await TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MatButtonModule,
        MatIconModule,
        MatInputModule,
        ReactiveFormsModule,
        RouterTestingModule,
      ],
      declarations: [LoginFormComponent],
      providers: [{ provide: AuthService, useValue: authService }],
    }).compileComponents()

    fixture = TestBed.createComponent(LoginFormComponent)
    component = fixture.componentInstance

    fixture.detectChanges()
  })

  const fillForm = () => {
    component.form.setValue({
      username: USERNAME,
      password: PASSWORD,
    })

    component.form.markAsDirty()
  }

  const findEl = (selector: string) => {
    const debugElement = fixture.debugElement.query(
      By.css(`[data-testid=${selector}]`),
    )

    if (!debugElement) {
      throw new Error(`queryByCss: Element with ${selector} not found`)
    }

    return debugElement
  }

  const dispatchFakeEvent = (
    element: EventTarget,
    type: string,
    bubbles: boolean = false,
  ) => {
    const event = document.createEvent('Event')
    event.initEvent(type, bubbles, false)
    element.dispatchEvent(event)
  }

  const setFieldElementValue = (
    element: HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement,
    value: string,
  ) => {
    element.value = value
    // Dispatch an `input` or `change` fake event
    // so Angular form bindings take notice of the change.
    const isSelect = element instanceof HTMLSelectElement
    dispatchFakeEvent(
      element,
      isSelect ? 'change' : 'input',
      isSelect ? false : true,
    )
  }

  const setFieldValue = <T>(testId: string, value: string) => {
    setFieldElementValue(findEl(testId).nativeElement, value)
  }

  const makeClickEvent = (target: EventTarget): Partial<MouseEvent> => {
    return {
      preventDefault(): void {},
      stopPropagation(): void {},
      stopImmediatePropagation(): void {},
      type: 'click',
      target,
      currentTarget: target,
      bubbles: true,
      cancelable: true,
      button: 0,
    }
  }

  const click = (testId: string) => {
    const element = findEl(testId)
    const event = makeClickEvent(element.nativeElement)
    element.triggerEventHandler('click', event)
  }

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('submits the form successfully', async () => {
    expect(findEl('submit-button').properties['disabled']).toBe(true)

    fillForm()

    fixture.detectChanges()

    expect(findEl('submit-button').properties['disabled']).toBe(false)

    findEl('form').triggerEventHandler('submit', {})

    fixture.detectChanges()

    expect(authService.login).toHaveBeenCalledWith(USERNAME)
  })

  it('does not submit an invalid form', async () => {
    findEl('form').triggerEventHandler('submit', {})

    expect(authService.login).not.toHaveBeenCalled()
  })

  it('toggles the password display', async () => {
    setFieldValue('password', 'top secret')

    const passwordEl = findEl('password')
    expect(passwordEl.attributes['type']).toBe('password')

    click('show-password')
    fixture.detectChanges()

    expect(passwordEl.attributes['type']).toBe('text')

    click('show-password')
    fixture.detectChanges()

    expect(passwordEl.attributes['type']).toBe('password')
  })
})

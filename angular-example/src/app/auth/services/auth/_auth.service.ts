import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { BehaviorSubject, Observable } from 'rxjs'

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private _username!: string

  private _isLogged$: BehaviorSubject<boolean>

  isLogged$: Observable<boolean>

  constructor(private readonly _router: Router) {
    this._isLogged$ = new BehaviorSubject<boolean>(false)
    this.isLogged$ = this._isLogged$.asObservable()
  }

  login(username: string): void {
    this._username = username
    this._isLogged$.next(true)
    // this._router.navigate([''])
  }

  getUser(): string {
    return this._username
  }
}
